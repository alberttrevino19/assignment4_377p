#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <cmath>        // std::abs
#include<bits/stdc++.h> 

using namespace std;

//tuple struct
struct COOtuple {
        int row; // this node
        int column; // end node
        int value; // edge weight
    };

struct rankNode{
    int id;
    float rank;
};

//function declarations
void constr_CSR(vector<COOtuple> dimacs_rep, int *row_index, int *col_index, int *values, int numNodes);
void print_CSR_vectors(int *row_index, int *col_index, int *values);
void CSR_to_DIMACS(int *row_index, int *col_index, int *values);
void page_rank(int row_index[], int col_index[], int numEdges, int numNodes);
void makeHistogram(int *row_index);




int main(int argc, char *argv[]) {

    ifstream inFile;

    string fileName;
    fileName = argv[1];
    
    inFile.open(fileName);

    if (!inFile) {
        cout << "Unable to open file";
        exit(1); // terminate with error
    }

    string trash; // string to simply collect trash values, not to be used
    inFile >> trash; // discard first two entries
    inFile >> trash;

    int numNodes;
    inFile >> numNodes;
    int numEdges;
    inFile >> numEdges;

    vector<COOtuple> listOfConnections; // basically an array, but size is dynamic

    int isPrint = stoi(argv[2]);

    
    string label;

    string commentFlag = "c";
    string arcFlag = "a";

    while (inFile >> label) {
        if (label.compare(arcFlag) == 0)
        {
            int x, y, z;
            COOtuple thisConnection; // creates an instance of a struct
            inFile >> x;
            inFile >> y;
            inFile >> z;

            thisConnection.row = x; // populate its fields with the data from file
            thisConnection.column = y;
            thisConnection.value = z;
            listOfConnections.push_back(thisConnection); // add to back of vector

        }
        else if (label.compare(commentFlag));
        {
            getline(inFile, trash);
        }
        
    }

    //Sort COO vector list by row least to greatest
    struct{
        bool operator()(COOtuple a, COOtuple b) const{
            if(a.row == b.row){
                return a.column < b.column;
            }
            return a.row < b.row;
        }
    } lTOg;

    sort(listOfConnections.begin(), listOfConnections.end(), lTOg);

    //check for duplicates ONLY NEED TO FOR RMAT *ONLY WORKS IF SORTED LISTED*
    vector<COOtuple>::iterator currentTuple;
    int position = 0;
    int dontAdvance = 0;
    for(currentTuple = listOfConnections.begin(); distance(currentTuple, listOfConnections.end()) > 1;){
        //check the current tuple and the one next to it if they are the same
        auto oldTuple = currentTuple;
        auto nextTuple = ++currentTuple;
       
        if((oldTuple->row == nextTuple->row) && (oldTuple->column == nextTuple->column)){ //if they are the same\
            //remove the tuple that has the lesser value
            if(oldTuple->value > nextTuple->value){ 
                currentTuple = listOfConnections.erase(currentTuple);
                dontAdvance = 0;
            }
            else if(oldTuple->value <= nextTuple->value){
                listOfConnections.erase(oldTuple);
                --currentTuple;
            }
        }
    }

    numEdges = listOfConnections.size();

    int values[numEdges+2];

    int col_index[numEdges+2];

    int row_index[numNodes+2];

    constr_CSR(listOfConnections, row_index, col_index, values, numNodes);
    if(isPrint){
        print_CSR_vectors(row_index, col_index, values);
    }


    page_rank(row_index, col_index, numEdges, numNodes);
    CSR_to_DIMACS(row_index, col_index, values);
    makeHistogram(row_index);

    inFile.close();
    return 0;
}

//outputs info to a csv file to be able to be opened in excel or libreoffice already parsed
void makeHistogram(int *row_index){
    int iter = 0;
    int rIndex;
    int cIndex;
    ofstream outFile;
    outFile.open("histogram.csv");
    outFile << endl;
    for(rIndex = 1; row_index[rIndex+2] != -1; rIndex++){

        if(row_index[rIndex] != 0){ //if there is anything on the current row
            int incr = 1;
            cIndex = row_index[rIndex]; //current column index
            int nxtRowIndex = row_index[rIndex + incr]; //next row index to calculate the difference

            while(nxtRowIndex == 0){
                incr++;
                nxtRowIndex = row_index[rIndex + incr];
            }

            if(nxtRowIndex == -1){
                nxtRowIndex = row_index[rIndex];
            }
            else if(row_index[rIndex + incr + 1] == -1){
                nxtRowIndex = row_index[rIndex] + 1;
            }

            iter = nxtRowIndex - row_index[rIndex]; //calculate row difference how many outgoing nodes

        }
       outFile << rIndex << ", " << iter << endl;
    }

    if(row_index[rIndex] != 0){
        iter = 1;
    }
    else{
        iter = 0;
    }

    outFile << rIndex << ", " << iter << endl;
    outFile.close();

}

/*Constructs the Compressed Sparse Row representation given the DIMACS formatting*/
void constr_CSR(vector<COOtuple> dimacs_rep, int *row_index, int *col_index, int *values, int numNodes){
    //Start constructing the CSR Representation

    //create value array
    vector<COOtuple>::iterator it;
    int i = 1;
    for(it = dimacs_rep.begin(); it < dimacs_rep.end(); it++){
        values[i] = (it->value);
        i++;
    }
    values[i] = -1;

    //create col_index array
    vector<COOtuple>::iterator it2;
    i = 1;
    for(it2 = dimacs_rep.begin(); it2 < dimacs_rep.end(); it2++){
        col_index[i] = (it2->column);
        i++;
    }
    col_index[i] = -1;


    //create the row_index array
    int prevRow = 0;

    int rowIndex = 1;
    int columnIndex = 1;

    vector<COOtuple>::iterator it3;
    it3 = dimacs_rep.begin();


    while(it3 < dimacs_rep.end()){ //iterate through COO list

        if(it3->row == rowIndex && it3->row != prevRow){ //is this node on a new row?
            row_index[rowIndex] = (columnIndex);
            columnIndex++;
            prevRow = it3->row;
            it3++;
        }
        else if(it3->row == prevRow){ //if it's on the same row
            it3++;
            columnIndex++;
            rowIndex--;
        }
        else if(it3->row != rowIndex){ //if there's nothing on this row
            row_index[rowIndex] = 0;
        }
        
        rowIndex++;
    }
    row_index[0] = numNodes;
    row_index[rowIndex] = row_index[rowIndex-1]; //store numEdges
    row_index[rowIndex+1] = -1; //null terminate it

}

//prints out the CSR vectors
void print_CSR_vectors(int *row_index, int *col_index, int *values){

    cout << "CSR Representation:" << endl;
    //print row_index array
        cout << "Row Indices: [";
        int i = 1;
        while(row_index[i+1] != -1){
            cout << row_index[i] << ", ";
            i++;
        }
        cout << row_index[i] << "]";
        cout << endl;

    //print col_index array
        cout << "Column Indices: [";
        i = 1;
        while(col_index[i+1] != -1){
            cout << col_index[i] << ", ";
            i++;
        }
        cout << col_index[i] << "]";
        cout << endl;

    //print value array
        cout << "Values: [";
        i = 1;
        while(values[i+1] != -1){
            cout << values[i] << ", ";
            i++;
        }
        cout << values[i] << "]";
        cout << endl;
     
}

void CSR_to_DIMACS(int *row_index, int *col_index, int *values){

    int numNodes, numEdges;

    ofstream outFile;
    outFile.open("CSR_TO_DIMACS.dimacs");

    //initial stuff
    outFile << "p sp ";

    numNodes = row_index[0];

    //number of nodes
    outFile << numNodes << " ";

    //number of edges
    int itx;
    for(itx = 1; row_index[itx+1] != -1; itx++){

    }
    outFile << row_index[itx] << endl;

    //Create a list with all the nodes
    vector<COOtuple> thisList;
    int iter;
    int cIndex;
    int rIndex;
    //loop through the row_index array
    for(rIndex = 1; row_index[rIndex+2] != -1; rIndex++){

        if(row_index[rIndex] != 0){ //if there is anything on the current row
            int incr = 1;
            cIndex = row_index[rIndex]; //current column index
            int nxtRowIndex = row_index[rIndex + incr]; //next row index to calculate the difference

            while(nxtRowIndex == 0){
                incr++;
                nxtRowIndex = row_index[rIndex + incr];
            }

            if(nxtRowIndex == -1){
                nxtRowIndex = row_index[rIndex];
            }
            else if(row_index[rIndex + incr + 1] == -1){
                nxtRowIndex = row_index[rIndex] + 1;
            }

            iter = nxtRowIndex - row_index[rIndex]; //calculate row difference

            while(iter > 0){
                COOtuple thisTuple;
                thisTuple.row = rIndex;
                thisTuple.column = col_index[cIndex];
                thisTuple.value = values[cIndex];

                thisList.push_back(thisTuple);
                cIndex++;
                iter--;
            }
        }
    }

    COOtuple thisTuple;
    thisTuple.row = rIndex;
    thisTuple.column = col_index[row_index[rIndex]];
    thisTuple.value = values[row_index[rIndex]];
    thisList.push_back(thisTuple);

    //output thisList to file
     vector<COOtuple>::iterator itr; 
        for (itr = thisList.begin(); itr < thisList.end(); itr++) {
            outFile << "a ";
            outFile << itr->row << " "; 
            outFile << itr->column << " ";
            outFile << itr->value << endl; 
        }

    outFile.close();
}

//calculating page rank
//Push style updates all outgoing nodes of the current node
void page_rank(int *row_index, int *col_index, int numEdges, int numNodes){

    float *currRanks; 
    currRanks = (float *) malloc(sizeof(int) * numNodes+2);
    float *newRanks; 
    newRanks = (float *) malloc(sizeof(int) * numNodes+2);
    float stoppingValue = 1;
    float diff = 0;
    int iter;

    float d = .85;

    int i;
    for(i = 1; i < numNodes+1; i++){
        currRanks[i] = ((float) 1) / numNodes;
    }

    currRanks[i] = -1;


    int iterations = 0;

    while(stoppingValue){
        iterations++;
        //initialize every page's new rank to  except nodes that don't have any incoming nodes
        bool isFound;
        int k;
        for(i = 1; i < numNodes+1; i++){
            newRanks[i] = ((float)(1 - d)) / numNodes;
        }
        newRanks[i] = -1;

        int cIndex;
        int nodeID;
        //iterate through row_index list
        for(nodeID = 1; row_index[nodeID+1] != -1; nodeID++){
            cIndex = row_index[nodeID]; //current column index, the value inside the row_index array
            //if there is anything on the current row

            if(cIndex != 0){

                float sum = 0;
                //Calculate the number of nodes that connect to this one by iterating through column_index and incrementing for each find
                int incr = 1;
                int nxtRowIndex = row_index[nodeID + incr]; //next row index to calculate the difference

                while(nxtRowIndex == 0){
                    incr++;
                    nxtRowIndex = row_index[nodeID + incr];
                }

                if(nxtRowIndex == -1){
                    nxtRowIndex = row_index[nodeID];
                }
                else if(row_index[nodeID + incr + 1] == -1){
                    nxtRowIndex = row_index[nodeID] + 1;
                }

                iter = nxtRowIndex - row_index[nodeID]; //calculate number of Outgoing nodes for current node
                float numOutgoing = iter;
                while(iter > 0){//for each outgoing node
                    newRanks[col_index[cIndex]] += (d * (currRanks[nodeID] / numOutgoing));
                    cIndex++;
                    iter--;
                }

            }
                    
        }

        //changing every score to it's new score
        int count = 0;
        for(i = 1; i < numNodes+1; i++){
            diff = abs(newRanks[i] - currRanks[i]);
            if(diff < .0001){
                count++;

            }
            currRanks[i] = newRanks[i];
        }

        if(count == numNodes){
            stoppingValue = 0;
        }
        
    }

    float rankSum = 0;
    int k;
    for(k = 1; currRanks[k] != -1; k++){

        rankSum += currRanks[k];

    }

    //print nodeID and node label for each node
    ofstream outFile;
    outFile.open("pageRankPrint.txt");

    vector<rankNode> finalRanks;
    rankNode thisNode;
    for(k = 1; currRanks[k] != -1; k++){
        currRanks[k] = currRanks[k] / rankSum;
        thisNode.id = k;
        thisNode.rank = currRanks[k];
        finalRanks.push_back(thisNode);
    }

    //Sort ranks vector list by rank greatest to least or ID in ascending

    struct{
        bool operator()(rankNode a, rankNode b) const{
            if(a.rank == b.rank){
                return a.id < b.id;
            }
            return a.rank > b.rank;
        }
    } gtol;

    sort(finalRanks.begin(), finalRanks.end(), gtol);

    //output each node and it's rank to pageRankPrint.txt

    vector<rankNode>::iterator it3;

    for(it3 = finalRanks.begin(); it3 != finalRanks.end(); it3++){

        outFile << "nodeID: " << it3->id << " rank: " << fixed << setprecision(6) << it3->rank << endl;

    }
    

    outFile.close();

    //get 10 highest page ranks and output their ID's

    ofstream outFile2;
    outFile2.open("top10.txt");

    int counter = 0;
    it3 = finalRanks.begin();
    float prevRank = 0;
    while(it3 != finalRanks.end() && counter != 10){
        if(it3->rank != prevRank){
            counter++;
            prevRank = it3->rank;
        }
        outFile2 << "nodeID: " << it3->id << " rank: " << fixed << setprecision(6) << it3->rank << endl;
        it3++;
    }

    outFile2.close();
    
}