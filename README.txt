# assignment4_377p
Assignment 4 for Programming for Performance

Albert C. Trevino
act2534
Nathan Gates
nag2486

To compile our program simply run the command 'make' while in the folder directory.

After running make, execute it by executing the command 'graph' with two arguements
													
				graph	file	bool

where:

 'file' is the file, in DIMACS format, to be accessed and compressed

 'int' is a true (1) or false (0) value if you would like to see the arrays that are created from compressing to CSR format



For bullet point one, the bool argument is used when you would like to see the CSR representation.
For bullet point two, the desired output will always be output into a new .dimacs file created called 'CSR_TO_DIMACS.dimacs'

Regarding Page Rank: the node ID's and ranks will always be ouput into a new .txt file created called 'pageRankPrint.txt'

For the pagerank expiriment, the top10 values are output into a file called 'top10.txt'

HISTOGRAMS NOTE:
	
	The histograms we created are super blurry and the values are super small for the bigger dimacs values.	
	We promise that every node is there, even though it's tough to see. We output the data to 'histogram.csv', 
	if you would like to confirm the values. 

